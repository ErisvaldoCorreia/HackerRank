package hackerRank;

import java.util.*;

/*
 * Author: Erisvaldo Correia
 * 2018
 */
public class SolveMeFirst {

	static int solveMeFirst(int a, int b) {
		return a + b;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int a;
		a = in.nextInt();
		int b;
		b = in.nextInt();
		System.out.println(solveMeFirst(a, b));
	}

}
