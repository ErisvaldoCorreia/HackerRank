/*
 * Author: Erisvaldo Correia
 * 2018
 */

// Day0 - Data Types Javascript

function types(secondInteger, secondDecimal, secondString) {
    
    const firstInteger = 4;
    console.log(firstInteger + parseInt(secondInteger));
    
    const firstDecimal = 4.0;
    console.log(firstDecimal + parseFloat(secondDecimal));
    
    const firstString = 'HackerRank ';
    console.log(firstString + secondString);
    
};

types(5, 4.78, 'is the best');