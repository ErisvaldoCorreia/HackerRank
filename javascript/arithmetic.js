/*
 * Author: Erisvaldo Correia
 * 2018
 */

// Day1 - Arithmétic Operators Javascript

function getArea(length, width) {
	
    let area;
    area = length * width;
    return area;
    
}

function getPerimeter(length, width) {
    
	let perimeter;
    perimeter = 2 * (length + width);
    return perimeter;
    
}

const area = getArea(4, 7);
const perimeter = getPerimeter(4, 7);

console.log('A area é = ' + area);
console.log('O perimetro é = ' + perimeter);
