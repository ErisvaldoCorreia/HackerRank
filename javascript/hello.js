/*
 * Author: Erisvaldo Correia
 * 2018
 */

// Day0 - Hello World Javascript

function greeting(name) {
    
    console.log('Hello, World!');
    console.log(name);
    
};

greeting('Erisvaldo');
